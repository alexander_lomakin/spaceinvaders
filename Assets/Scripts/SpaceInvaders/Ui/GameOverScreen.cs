﻿using UnityEngine;
using UnityEngine.UI;

namespace SpaceInvaders.Ui
{
    public class GameOverScreen : MonoBehaviour
    {
        public Text scoreCaption;

        public void Initialize(int score)
        {
            scoreCaption.text = string.Format(scoreCaption.text, score);
        }
    }
}

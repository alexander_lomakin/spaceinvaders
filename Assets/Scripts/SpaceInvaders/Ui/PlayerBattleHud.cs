﻿using SpaceInvaders.Systems;
using UnityEngine;
using UnityEngine.UI;

namespace SpaceInvaders.Ui
{
    public class PlayerBattleHud : MonoBehaviour
    {
        public Text score;
        public Text highscore;
        public Text lives;

        private int _score;
        private int _highscore;

        private IScoreStorage _scoreStorage;

        public void Initalize(IScoreStorage scoreStorage)
        {
            _scoreStorage = scoreStorage;
        }
        
        private void OnEnable()
        {
            _highscore = PlayerPrefs.GetInt("highscore");
        }

        // ToDo: In real world need to update text comps only when values changed
        private void Update()
        {
            _score = _scoreStorage.KillScore;
            _highscore = Mathf.Max(_highscore, _score);

            lives.text = _scoreStorage.PlayerLives.ToString();
            score.text = _score.ToString();
            highscore.text = _highscore.ToString();
        }

        private void OnDestroy()
        {
            PlayerPrefs.SetInt("highscore", Mathf.Max(_score, _highscore));
            PlayerPrefs.Save();
        }
    }
}

﻿using System;
using UnityEngine;

namespace SpaceInvaders.Ui
{
    public class StartScreen : MonoBehaviour
    {
        public event Action StartGame = delegate { };
        private void Update()
        {
            if (Input.GetKeyUp(KeyCode.Space))
            {
                StartGame();
                gameObject.SetActive(false);
            }
        }
    }
}

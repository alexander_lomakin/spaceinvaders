using SpaceInvaders.Components;
using UnityEngine;

namespace SpaceInvaders.Data
{
    [CreateAssetMenu(fileName = "New spaceships collection", menuName = "Data/new spaceships collection", order = 0)]
    public class Spaceships : ScriptableObject
    {
        public SpaceshipShell[] shells;

        public SpaceshipShell GetShell(SpaceshipType type)
        {
            for (var i = 0; i < shells.Length; i++)
            {
                if (shells[i].type == type)
                    return shells[i];
            }

            return null;
        }
    }
}
using System.Collections.Generic;
using UnityEngine;

namespace SpaceInvaders.Data
{
    // Here lays all level data and monsters info, It no problem to read this structure from file/backend, also
    // I can create multiple level settings with different parameters without problems,
    // for prototype purposes it placed in Scriptable objects
    [CreateAssetMenu(fileName = "New level details", menuName = "Data/new level details", order = 0)]
    public class LevelDetails : ScriptableObject
    {
        public Vector2 monsterShootInterval;
        public float monsterShotChance;
        public Vector2[] startPlayerPositions;
        public Vector2[] guardStarshipPositions;
        public Vector2 bossSpawnPosition;
        public Vector2 upperLeftBorder;
        public Vector2 lowerRightBorder;
        public float spawnBonusShipInterval;
        public float monsterMoveNextRowDistance;
        public float monsterStartY;
        public float bonusShipStartY;
        public float monsterRowHeight;
        public float monsterRowWidth;

        public int monsterRowSize;
        public List<SpaceshipInfo> monsterRows;
        public SpaceshipInfo bonusSpaceshipInfo;
        public SpaceshipInfo guardSpaceshipInfo;
        public SpaceshipInfo bossSpaceshipInfo;

        public Vector2 LevelCenter
        {
            get
            {
                var x = upperLeftBorder.x + (lowerRightBorder.x - upperLeftBorder.x) / 2;
                var y = upperLeftBorder.y + (lowerRightBorder.y - upperLeftBorder.y) / 2;

                return new Vector2(x, y);
            }
        }
    }
}
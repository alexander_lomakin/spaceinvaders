using SpaceInvaders.Components;
using UnityEngine;

namespace SpaceInvaders.Data
{
    [CreateAssetMenu(fileName = "New spaceship info", menuName = "Data/new spaceship info", order = 0)]
    public class SpaceshipInfo : ScriptableObject
    {
        public int health;
        public int damage;
        public int score;
        public bool killableByBorders;
        public DamageType dmgType;
        public DamageType absorbDmg;
        public float maxSpeed;
        public float slowdownSpeed;
        public float gunCooldown;
        public float gunBulletSpeed;
        public Vector2 size;
        public SpaceshipShell shell;
    }
}
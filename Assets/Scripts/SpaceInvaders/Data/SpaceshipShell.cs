using SpaceInvaders.Components;
using UnityEngine;

namespace SpaceInvaders.Data
{
    [CreateAssetMenu(fileName = "New Spaceship shell", menuName = "Data/New spacheship shell")]
    public class SpaceshipShell : ScriptableObject
    {
        public Sprite shellSprite;
        public SpaceshipType type;
    }
}
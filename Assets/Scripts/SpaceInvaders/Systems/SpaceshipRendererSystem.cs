using System.Collections.Generic;
using SpaceInvaders.Components;
using SpaceInvaders.Utils;
using SpaceInvaders.Views;
using Unity.Entities;

namespace SpaceInvaders.Systems
{
    [AlwaysUpdateSystemAttribute]
    public class SpaceshipRendererSystem : ComponentSystem
    {
        public Pool spaceshipViewsPool;

        private Dictionary<Entity, SpaceshipView> _cachedViews = new Dictionary<Entity, SpaceshipView>();

        protected override void OnUpdate()
        {
            // ToDo: Temporal solution with allocation, need to use DOTS rendering utilities
            foreach (var pair in _cachedViews)
            {
                pair.Value.entityExistFlag = false;
            }
            
            Entities.ForEach((Entity entity, ref PositionAndRotation positionAndRotation, ref ShipShell shell) =>
            {
                var view = InstantiateView(entity);
                
                view.Initialize(shell.spaceshipType);

                view.transform.position = positionAndRotation.position;
            });
            
            // ToDo: Temporal solution with allocation, need to use DOTS rendering utilities
            var toRemove = new List<Entity>();
            
            foreach (var pair in _cachedViews)
            {
                if (!pair.Value.entityExistFlag)
                {
                    spaceshipViewsPool.Put(pair.Value.gameObject);

                    toRemove.Add(pair.Key);
                }
            }

            for (var i = 0; i < toRemove.Count; i++)
            {
                _cachedViews.Remove(toRemove[i]);
            }
        }

        private SpaceshipView InstantiateView(Entity entity)
        {
            if (_cachedViews.ContainsKey(entity))
                return _cachedViews[entity];

            var view = spaceshipViewsPool.Take().GetComponent<SpaceshipView>();

            _cachedViews.Add(entity, view);

            return view;
        }
    }
}
using SpaceInvaders.Components;
using SpaceInvaders.Data;
using Unity.Entities;
using UnityEngine;

namespace SpaceInvaders.Systems
{
    public class MovementSystem : ComponentSystem
    {
        public LevelDetails levelDetails;

        private float _leftBorder;
        private float _rightBorder;
        private float _upperBorder;
        private float _lowerBorder;
        
        public void Initialize()
        {
            _leftBorder = levelDetails.upperLeftBorder.x;
            _rightBorder = levelDetails.lowerRightBorder.x; 
            _upperBorder = levelDetails.upperLeftBorder.y;
            _lowerBorder = levelDetails.lowerRightBorder.y;
        }

        protected override void OnUpdate()
        {
            var deltaTime = Time.deltaTime;

            Entities.ForEach((Entity entity, ref PositionAndRotation positionAndRotation, ref Velocity velocity) =>
            {
                positionAndRotation.position += deltaTime * velocity.speed * velocity.velocity;

                velocity.speed = Mathf.Clamp(velocity.speed - velocity.slowdownSpeed * deltaTime, 0f, velocity.maxSpeed);
               
                positionAndRotation.position.x = Mathf.Clamp(positionAndRotation.position.x, _leftBorder, _rightBorder);
                positionAndRotation.position.y = Mathf.Clamp(positionAndRotation.position.y, _lowerBorder, _upperBorder);
            });
        }
    }
}
using SpaceInvaders.Components;
using Unity.Entities;
using UnityEngine;

namespace SpaceInvaders.Systems
{
    // Handle collisions between spaceships
    public class SpaceshipCollisionSystem : ComponentSystem
    {
        private const int DamageOnCollision = 1;
        
        protected override void OnUpdate()
        {
            Entities.ForEach((Entity entity, ref SpaceshipDriver driver, ref PositionAndRotation positionAndRotation, ref Size size) =>
            {
                if (driver.playerId != SpaceshipFactory.MonsterId)
                    return;
                
                ProcessCollisionsForMonsterSpaceship(entity, positionAndRotation.position, size.width, size.height);
            });
        }
        
        private void ProcessCollisionsForMonsterSpaceship(Entity monsterEntity, Vector2 monsterPos, float width, float height)
        {
            Entities.ForEach((Entity entity, ref PositionAndRotation posAndRot, ref Size size, ref Health health) =>
            {
                if (entity == monsterEntity)
                    return;
                
                if (BoxInterception(monsterPos, new Vector2(width, height),  posAndRot.position, new Vector2(width, height)))
                {
                    health.health -= DamageOnCollision;
                    
                    if (health.health <= 0)
                    {
                        EntityManager.DestroyEntity(entity);
                    }
                }
            });
        }
        
        // Custom physics, because ECS binding with unity physics 2d is new for me
        private bool BoxInterception(Vector2 boxACenter, Vector2 boxASize, Vector2 boxBCenter, Vector2 boxBSize)
        {
            var boxAHalfX = boxASize.x / 2;
            var boxBHalfX = boxBSize.x / 2;

            var boxALeftX = boxACenter.x - boxAHalfX;
            var boxARightX = boxACenter.x + boxAHalfX;
            var boxBLeftX = boxBCenter.x - boxBHalfX;
            var boxBRightX = boxBCenter.x + boxBHalfX;
            
            var passX = (boxBLeftX >= boxALeftX && boxBLeftX <= boxARightX) ||
                         (boxBRightX >= boxALeftX && boxBRightX <= boxARightX)
                || (boxBLeftX <= boxALeftX && boxBRightX >= boxARightX);

            if (!passX)
                return false;
            
            var boxAHalfY = boxASize.y / 2;
            var boxBHalfY = boxBSize.y / 2;

            var boxALeftY = boxACenter.y - boxAHalfY;
            var boxARightY = boxACenter.y + boxAHalfY;
            var boxBLeftY = boxBCenter.y - boxBHalfY;
            var boxBRightY = boxBCenter.y + boxBHalfY;
            
            var passY = (boxBLeftY >= boxALeftY && boxBLeftY <= boxARightY) ||
                        (boxBRightY >= boxALeftY && boxBRightY <= boxARightY)
                        || (boxBLeftY <= boxALeftY && boxBRightY >= boxARightY);

            if (!passY)
                return false;
            
            return true;
        }
    }
}
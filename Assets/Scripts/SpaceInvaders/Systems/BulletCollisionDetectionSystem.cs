using SpaceInvaders.Components;
using Unity.Entities;
using UnityEngine;

namespace SpaceInvaders.Systems
{
    public interface IScoreStorage
    {
        int KillScore { get; }
        int PlayerLives { get; }
    }
    
    public class BulletCollisionDetectionSystem : ComponentSystem, IScoreStorage
    {
        public int KillScore { get; private set; }

        public int PlayerLives
        {
            get
            {
                int playerLives = 0;
                
                Entities.ForEach((Entity entity, ref SpaceshipDriver driver, ref Health health) =>
                {
                    if (driver.playerId == SpaceshipFactory.MonsterId)
                        return;
                    
                    playerLives = health.health;
                });

                return playerLives;
            }
        }
        
        protected override void OnUpdate()
        {
            Entities.ForEach((Entity entity, ref PositionAndRotation positionAndRotation, ref Damage damage) =>
            {
                ProcessCollisionsForBullet(entity, positionAndRotation.position, damage.damage, damage.dmgType);
            });
        }

        private void ProcessCollisionsForBullet(Entity bulletEntity, Vector2 bulletPos, int dmg, DamageType dmgType)
        {
            Entities.ForEach((Entity entity, ref PositionAndRotation posAndRot, ref Size size, ref Health health, ref Score score) =>
            {
                if (health.absorbDmg != dmgType)
                    return;
                
                if (PointInsideBox(bulletPos, posAndRot.position, size))
                {
                    health.health -= dmg;
                    
                    if (health.health <= 0)
                    {
                        KillScore += score.value;
                        
                        EntityManager.DestroyEntity(entity);
                    }
                    
                    EntityManager.DestroyEntity(bulletEntity);
                }
            });
        }
        
        // Custom physics, because ECS binding with unity physics 2d is new for me
        private bool PointInsideBox(Vector2 point, Vector2 boxCenter, Size boxSize)
        {
            var halfX = boxSize.width / 2;

            if (point.x < boxCenter.x - halfX || point.x > boxCenter.x + halfX)
                return false;

            var halfY = boxSize.height / 2;

            if (point.y < boxCenter.y - halfY || point.y > boxCenter.y + halfY)
                return false;
            
            return true;
        }
    }
}
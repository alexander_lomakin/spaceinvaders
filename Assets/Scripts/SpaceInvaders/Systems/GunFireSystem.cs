using SpaceInvaders.Components;
using Unity.Entities;
using UnityEngine;

namespace SpaceInvaders.Systems
{
    public class GunFireSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            var deltaTime = Time.deltaTime;

            Entities.ForEach((Entity entity, ref Gun gun) =>
            {
                gun.shotTimer += deltaTime;
            });
            
            Entities.ForEach((Entity entity, ref PlayerInput input) =>
            {
                if (input.cmd != InputCmd.Shoot)
                    return;
                
                ProcessFire(input.playerId);
                EntityManager.RemoveComponent(entity, typeof(PlayerInput));
            });
        }

        private void ProcessFire(byte playerId)
        {
            Entities.ForEach((Entity entity, ref SpaceshipDriver driver, ref PositionAndRotation positionAndRotation, ref Gun gun) =>
            {
                if (playerId != driver.playerId || gun.shotTimer <= gun.shotDuration)
                    return;

                gun.shotTimer = 0f;
                
                SpawnBullet(positionAndRotation.position, positionAndRotation.rotation, gun.bulletSpeed, gun.damage, gun.dmgType);
            });
        }

        public void SpawnBullet(Vector2 gunPosition, Vector2 gunDirection, float bulletSpeed, int bulletDmg, DamageType dmgType)
        {
            var bulletEntity = EntityManager.CreateEntity();
                
            EntityManager.AddComponent(bulletEntity, new ComponentType(typeof(PositionAndRotation)));
            EntityManager.AddComponent(bulletEntity, new ComponentType(typeof(Velocity)));
            EntityManager.AddComponent(bulletEntity, new ComponentType(typeof(Damage)));
            EntityManager.AddComponent(bulletEntity, new ComponentType(typeof(KillableByBorders)));
                
            EntityManager.SetComponentData(bulletEntity, new PositionAndRotation()
            {
                position = gunPosition,
                rotation = gunDirection
            });
            
            EntityManager.SetComponentData(bulletEntity, new Velocity()
            {
                maxSpeed = bulletSpeed,
                speed = bulletSpeed,
                slowdownSpeed = 0f,
                velocity = gunDirection.normalized
            });
            
            EntityManager.SetComponentData(bulletEntity, new Damage()
            {
                damage = bulletDmg,
                dmgType = dmgType
            });
        }
    }
}
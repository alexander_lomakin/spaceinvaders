using SpaceInvaders.Components;
using Unity.Entities;
using UnityEngine;

namespace SpaceInvaders.Systems
{
    // Handle player input, here we can easy adda second...n players, just need to send unuque player id and handle another keys.
    public class InputSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                CreateCmd(InputCmd.Left, 0);
            }
            
            if (Input.GetKey(KeyCode.RightArrow))
            {
                CreateCmd(InputCmd.Right, 0);
            }
            
            if (Input.GetKeyDown(KeyCode.Space))
            {
                CreateCmd(InputCmd.Shoot, 0);
            }
        }

        private void CreateCmd(InputCmd inputCmd, byte player)
        {
            var cmdEntity = EntityManager.CreateEntity();
                
            EntityManager.AddComponent(cmdEntity, new ComponentType(typeof(PlayerInput), ComponentType.AccessMode.ReadOnly));
                
            EntityManager.SetComponentData(cmdEntity, new PlayerInput()
            {
                cmd = inputCmd,
                playerId = player
            });
        }
    }
}
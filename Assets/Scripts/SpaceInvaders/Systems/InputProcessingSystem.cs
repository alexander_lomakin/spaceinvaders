using SpaceInvaders.Components;
using Unity.Entities;
using UnityEngine;

namespace SpaceInvaders.Systems
{
    public class InputProcessingSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            Entities.ForEach((Entity entity, ref PlayerInput input) =>
            {
                ProcessInput(input.playerId, input.cmd);

                if (input.cmd != InputCmd.Shoot)
                {
                    EntityManager.RemoveComponent(entity, typeof(PlayerInput));
                }
            });
        }

        private void ProcessInput(byte processingPlayerId, InputCmd cmd)
        {
            Entities.ForEach((Entity entity, ref Velocity velocity, ref SpaceshipDriver driver) =>
            {
                if (driver.playerId != processingPlayerId)
                    return;

                switch (cmd)
                {
                    case InputCmd.Left:
                        velocity.velocity = Vector2.left.normalized;
                        velocity.speed = velocity.maxSpeed;
                        break;
                    
                    case InputCmd.Right:
                        velocity.velocity = Vector2.right.normalized;
                        velocity.speed = velocity.maxSpeed;
                        break;
                }
            });
        }
    }
}
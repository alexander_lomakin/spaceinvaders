using SpaceInvaders.Components;
using SpaceInvaders.Data;
using Unity.Entities;
using UnityEngine;

namespace SpaceInvaders.Systems
{
    // If something reach level border this system will kill it.
    public class KillNearBordersSystem : ComponentSystem
    {
        public LevelDetails levelDetails;

        private float _leftBorder;
        private float _rightBorder;
        private float _upperBorder;
        private float _lowerBorder;
        
        public void Initialize()
        {
            _leftBorder = levelDetails.upperLeftBorder.x;
            _rightBorder = levelDetails.lowerRightBorder.x; 
            _upperBorder = levelDetails.upperLeftBorder.y;
            _lowerBorder = levelDetails.lowerRightBorder.y;
        }
        
        protected override void OnUpdate()
        {
            var deltaTime = Time.deltaTime;
            
            Entities.ForEach((Entity entity, ref KillableByBorders killable, ref PositionAndRotation positionAndRotation, ref Velocity velocity) =>
            {
                var pos = positionAndRotation.position + deltaTime * velocity.speed * velocity.velocity;

                if (pos.x <= _leftBorder || pos.x >= _rightBorder || pos.y >= _upperBorder || pos.y <= _lowerBorder)
                {
                    EntityManager.DestroyEntity(entity);
                }
            });
        }
    }
}
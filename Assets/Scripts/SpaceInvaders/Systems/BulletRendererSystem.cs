using System.Collections.Generic;
using SpaceInvaders.Components;
using SpaceInvaders.Utils;
using SpaceInvaders.Views;
using Unity.Entities;

namespace SpaceInvaders.Systems
{
    [AlwaysUpdateSystemAttribute]
    public class BulletRendererSystem : ComponentSystem
    {
        public Pool bulletViewsPool;

        private Dictionary<Entity, BulletView> _cachedViews = new Dictionary<Entity, BulletView>();
        
        protected override void OnUpdate()
        {
            // ToDo: Temporal solution with allocation, need to use DOTS rendering utilities
            foreach (var pair in _cachedViews)
            {
                pair.Value.entityExistFlag = false;
            }
            
            Entities.ForEach((Entity entity, ref PositionAndRotation positionAndRotation, ref Damage damage) =>
            {
                var view = InstantiateView(entity);

                view.entityExistFlag = true;
                
                view.transform.position = positionAndRotation.position;
            });
            
            // ToDo: Temporal solution with allocation, need to use DOTS rendering utilities
            var toRemove = new List<Entity>();
            
            foreach (var pair in _cachedViews)
            {
                if (!pair.Value.entityExistFlag)
                {
                    bulletViewsPool.Put(pair.Value.gameObject);
                    
                    toRemove.Add(pair.Key);
                }
            }

            for (var i = 0; i < toRemove.Count; i++)
            {
                _cachedViews.Remove(toRemove[i]);
            }
        }

        private BulletView InstantiateView(Entity entity)
        {
            if (_cachedViews.ContainsKey(entity))
                return _cachedViews[entity];

            var view = bulletViewsPool.Take().GetComponent<BulletView>();

            _cachedViews.Add(entity, view);

            return view;
        }
    }
}
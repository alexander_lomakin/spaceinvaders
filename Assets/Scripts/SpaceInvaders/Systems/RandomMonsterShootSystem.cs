using SpaceInvaders.Components;
using Unity.Entities;
using UnityEngine;
using Random = UnityEngine.Random;

namespace SpaceInvaders.Systems
{
    // Here we process regular wave monsters shooting, it was a bit of problem to check which monsters can shoot (only nearest
    // monsters without obstacles forward of them)
    public class RandomMonsterShootSystem : ComponentSystem
    {
        private Vector2 _shotInterval;
        private float _shotTimeElapsed;
        private float _shotTimeCooldown;
        private float _shotChance;
        private GunFireSystem _gunFireSystem;
        private Entity[] _nearestEntities;
        private float[] _nearestDistances;

        protected override void OnCreate()
        {
            base.OnCreate();
            
            _gunFireSystem = World.Active.GetExistingSystem(typeof(GunFireSystem)) as GunFireSystem;
        }

        public void Initialize(Vector2 shotInterval, int rowCount, float shotChance)
        {
            _shotInterval = shotInterval;
            _shotChance = shotChance;

            _shotTimeCooldown = Random.Range(shotInterval.x, shotInterval.y);
            
            _nearestDistances = new float[rowCount];
            _nearestEntities = new Entity[rowCount];
        }
        
        protected override void OnUpdate()
        {
            var deltaTime = Time.deltaTime;

            _shotTimeElapsed += deltaTime;
            
            if (_shotTimeElapsed < _shotTimeCooldown || _nearestEntities == null)
                return;

            _shotTimeElapsed = 0;
            _shotTimeCooldown = Random.Range(_shotInterval.x, _shotInterval.y);

            for (var i = 0; i < _nearestDistances.Length; i++)
            {
                _nearestDistances[i] = float.MaxValue;
                _nearestEntities[i] = Entity.Null;
            }
            
            Entities.ForEach((Entity entity, ref PositionAndRotation positionAndRotation, ref SpaceshipDriver driver, ref Row row) =>
            {
                if (driver.playerId != SpaceshipFactory.MonsterId)
                    return;
                
                if (positionAndRotation.position.y >= _nearestDistances[row.rowIndex])
                    return;

                _nearestEntities[row.rowIndex] = entity;
                _nearestDistances[row.rowIndex] = positionAndRotation.position.y;
            });

            for (var i = 0; i < _nearestDistances.Length; i++)
            {
                if (_nearestEntities[i] == Entity.Null || Random.Range(0, 1f) > _shotChance)
                    continue;
                
                var finalShooter = _nearestEntities[i];
                var shooterPos = EntityManager.GetComponentData<PositionAndRotation>(finalShooter);
                var shooterGun = EntityManager.GetComponentData<Gun>(finalShooter);

                _gunFireSystem.SpawnBullet(shooterPos.position, shooterPos.rotation, shooterGun.bulletSpeed, shooterGun.damage, shooterGun.dmgType);
            }
        }
    }
}
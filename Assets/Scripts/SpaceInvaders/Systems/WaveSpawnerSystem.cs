using System;
using SpaceInvaders.Components;
using Unity.Entities;
using UnityEngine;

namespace SpaceInvaders.Systems
{
    // Spawn regular waves and bonus spaceships
    [AlwaysUpdateSystemAttribute]
    public class WaveSpawnerSystem : ComponentSystem
    {
        public event Action SpawnWave = delegate { };
        public event Action SpawnBonusShip = delegate { };

        public float spawnBonusShipInterval;
        private float _spawnBonusShipElapsed;
        
        protected override void OnUpdate()
        {
            _spawnBonusShipElapsed += Time.deltaTime;

            if (_spawnBonusShipElapsed >= spawnBonusShipInterval)
            {
                SpawnBonusShip();
                _spawnBonusShipElapsed = 0;
                return;
            }
            
            var monstersCount = 0;
            Entities.ForEach((Entity entity, ref PositionAndRotation positionAndRotation, ref Velocity velocity, ref SpaceshipDriver driver) =>
            {
                if (driver.playerId != SpaceshipFactory.MonsterId)
                    return;

                monstersCount++;
            });

            if (monstersCount == 0)
            {
                Entities.ForEach((Entity entity, ref PositionAndRotation positionAndRotation, ref Health health) =>
                {
                    if (EntityManager.HasComponent<SpaceshipDriver>(entity))
                    {
                        var driver = EntityManager.GetComponentData<SpaceshipDriver>(entity);
                        
                        if (driver.playerId != SpaceshipFactory.MonsterId)
                            return;
                    }

                    EntityManager.DestroyEntity(entity);
                });
                
                SpawnWave();
            }
        }
    }
}
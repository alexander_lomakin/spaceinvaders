using SpaceInvaders.Components;
using SpaceInvaders.Data;
using Unity.Entities;
using UnityEngine;

namespace SpaceInvaders.Systems
{
    public enum BossFightStage : byte
    {
        None = 0,
        Appearance,
        AttackPlayer
    }
    
    // Handle all boss logic in one system, maybe it require specific class for boss AI?
    public class BossProcessingSystem : ComponentSystem
    {
        private Entity _bossEntity;
        private LevelDetails _levelDetails;
        private BossFightStage _stage;
        private GunFireSystem _gunFireSystem;
        private float _lastBulletElapsed;
        private float _shotCooldown;
        private Vector2 _shotCooldownRange = new Vector2(0.3f, 0.5f);

        protected override void OnCreate()
        {
            base.OnCreate();
            
            _gunFireSystem = World.Active.GetOrCreateSystem(typeof(GunFireSystem)) as GunFireSystem;
        }
        
        public void Initialize(LevelDetails levelDetails)
        {
            _levelDetails = levelDetails;
            _stage = BossFightStage.Appearance;
        }
        
        protected override void OnUpdate()
        {
            var deltaTime = Time.deltaTime;

            switch (_stage)
            {
                case BossFightStage.Appearance:
                    ProcessBossAppearance();
                    break;

                case BossFightStage.AttackPlayer:
                {
                    ProcessBulletRain(deltaTime);
                    ProcessMoveToPlayerDirection();

                    var world = World.Active;
                    var moveMonsterSystem = world.GetExistingSystem(typeof(MonsterMoverSystem));

                    if (moveMonsterSystem != null)
                    {
                        moveMonsterSystem.Enabled = !EntityManager.Exists(_bossEntity);
                    }

                    break;
                }
            }
        }

        private void ProcessBossAppearance()
        {
            Entities.ForEach((Entity entity, ref PositionAndRotation positionAndRotation, ref Velocity velocity, ref SpaceshipDriver driver) =>
            {
                if (driver.playerId != SpaceshipFactory.MonsterId)
                    return;

                EntityManager.DestroyEntity(entity);
            });
            
            _bossEntity = SpaceshipFactory.CreateBoss(World.Active, _levelDetails.bossSpawnPosition, Vector2.down, _levelDetails.bossSpaceshipInfo);
            
            _stage = BossFightStage.AttackPlayer;
        }

        private void ProcessBulletRain(float deltaTime)
        {
            if (!EntityManager.Exists(_bossEntity))
                return;

            _lastBulletElapsed += deltaTime;

            if (_lastBulletElapsed < _shotCooldown)
                return;

            _lastBulletElapsed = 0;
            _shotCooldown = Random.Range(_shotCooldownRange.x, _shotCooldownRange.y);

            var pos = EntityManager.GetComponentData<PositionAndRotation>(_bossEntity);
            var size = EntityManager.GetComponentData<Size>(_bossEntity);
            var gun = EntityManager.GetComponentData<Gun>(_bossEntity);
            var positionX = Random.Range(pos.position.x - size.width / 2, pos.position.x + size.width / 2);
            
            _gunFireSystem.SpawnBullet(new Vector2(positionX, pos.position.y), Vector2.down, gun.bulletSpeed, gun.damage, gun.dmgType);
        }

        private void ProcessMoveToPlayerDirection()
        {
            if (!EntityManager.Exists(_bossEntity))
                return;

            var playerX = 0f;
            Entities.ForEach((Entity entity, ref PositionAndRotation positionAndRotation, ref Velocity velocity, ref SpaceshipDriver driver) =>
            {
                if (driver.playerId == SpaceshipFactory.MonsterId)
                    return;

                playerX = positionAndRotation.position.x;
            });
            
            var bossPos = EntityManager.GetComponentData<PositionAndRotation>(_bossEntity);
            var bossVelocity = EntityManager.GetComponentData<Velocity>(_bossEntity);
            
            EntityManager.SetComponentData(_bossEntity, new Velocity()
            {
                maxSpeed = bossVelocity.maxSpeed,
                slowdownSpeed = bossVelocity.slowdownSpeed,
                speed = bossVelocity.maxSpeed,
                velocity = new Vector2(playerX - bossPos.position.x, 0).normalized
            });
        }
    }
}
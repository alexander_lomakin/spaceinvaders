using SpaceInvaders.Components;
using SpaceInvaders.Data;
using Unity.Entities;
using UnityEngine;

namespace SpaceInvaders.Systems
{
    // Move regular wave monsters here
    public class MonsterMoverSystem : ComponentSystem
    {
        public LevelDetails levelDetails;
        public float moveNextRowDistance;

        private float _leftBorder;
        private float _rightBorder;
        private bool _moveLeft;
        
        public void Initialize()
        {
            _leftBorder = levelDetails.upperLeftBorder.x;
            _rightBorder = levelDetails.lowerRightBorder.x;
        }
        
        protected override void OnUpdate()
        {
            var moveNextRow = false;
            var deltaTime = Time.deltaTime;
            
            Entities.ForEach((Entity entity, ref PositionAndRotation positionAndRotation, ref Velocity velocity, ref SpaceshipDriver driver) =>
            {
                if (driver.playerId != SpaceshipFactory.MonsterId)
                    return;

                var killable = EntityManager.HasComponent<KillableByBorders>(entity);
                
                velocity.speed = velocity.maxSpeed;

                if (!killable)
                {
                    velocity.velocity = _moveLeft ? Vector2.left : Vector2.right;
                }
                
                var predictPos = positionAndRotation.position + deltaTime * velocity.speed * velocity.velocity;

                if (!killable && (predictPos.x < _leftBorder || predictPos.x > _rightBorder))
                {
                    moveNextRow = true;
                    velocity.speed = 0;
                }
            });
            
            if (moveNextRow)
            {
                _moveLeft = !_moveLeft;
            }
            
            Entities.ForEach((Entity entity, ref PositionAndRotation positionAndRotation, ref Velocity velocity, ref SpaceshipDriver driver) =>
            {
                if (driver.playerId != SpaceshipFactory.MonsterId)
                    return;
                
                var killable = EntityManager.HasComponent<KillableByBorders>(entity);

                if (killable)
                    return;

                if (moveNextRow)
                {
                    positionAndRotation.position -= new Vector2(0, moveNextRowDistance);
                }
            });
        }
    }
}
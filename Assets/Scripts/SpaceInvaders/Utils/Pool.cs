using System.Collections.Generic;
using UnityEngine;

namespace SpaceInvaders.Utils
{
    public class Pool : MonoBehaviour
    {
        public GameObject prefab;
        public List<GameObject> objects;

        public GameObject Take()
        {
            if (objects.Count == 0)
            {
                var go = Instantiate(prefab);

                go.transform.SetParent(transform, false);
                
                return go;
            }

            var obj = objects[0];
            objects.RemoveAt(0);
            
            obj.gameObject.SetActive(true);

            return obj;
        }

        public void Put(GameObject obj, bool setParent = false)
        {
            objects.Add(obj);
            obj.gameObject.SetActive(false);

            if (setParent)
            {
                obj.transform.SetParent(transform, false);
            }
        }
    }
}
using Unity.Entities;

namespace SpaceInvaders.Components
{
    public struct Damage : IComponentData
    {
        public int damage;
        public DamageType dmgType;
    }
}
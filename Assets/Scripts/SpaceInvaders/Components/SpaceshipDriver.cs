using Unity.Entities;

namespace SpaceInvaders.Components
{
    public struct SpaceshipDriver : IComponentData
    {
        public short playerId;
    }
}
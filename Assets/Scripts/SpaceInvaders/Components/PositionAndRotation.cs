﻿using Unity.Entities;
using UnityEngine;

namespace SpaceInvaders.Components
{
    public struct PositionAndRotation : IComponentData
    {
        public Vector2 position;
        public Vector2 rotation;
    }
}

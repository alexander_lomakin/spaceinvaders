using Unity.Entities;
using UnityEngine;

namespace SpaceInvaders.Components
{
    public struct Velocity : IComponentData
    {
        public float maxSpeed;
        public float speed;
        public float slowdownSpeed;
        public Vector2 velocity;
    }
}
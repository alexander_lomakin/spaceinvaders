using Unity.Entities;

namespace SpaceInvaders.Components
{
    public enum SpaceshipType : byte
    {
        Player = 0,
        Guardship,
        Monster1,
        Monster2,
        Monster3,
        Bonus,
        Boss
    }
    
    public struct ShipShell : IComponentData
    {
        public SpaceshipType spaceshipType;
    }
}
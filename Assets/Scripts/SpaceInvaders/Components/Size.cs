using Unity.Entities;

namespace SpaceInvaders.Components
{
    public struct Size : IComponentData
    {
        public float width;
        public float height;
    }
}
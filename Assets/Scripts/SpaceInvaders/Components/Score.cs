using Unity.Entities;

namespace SpaceInvaders.Components
{
    public struct Score : IComponentData
    {
        public int value;
    }
}
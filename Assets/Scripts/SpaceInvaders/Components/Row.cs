using Unity.Entities;

namespace SpaceInvaders.Components
{
    public struct Row : IComponentData
    {
        public short rowIndex;
    }
}
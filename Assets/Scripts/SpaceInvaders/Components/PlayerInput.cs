using Unity.Entities;

namespace SpaceInvaders.Components
{
    public enum InputCmd : byte
    {
        None = 0,
        Left,
        Right,
        Shoot
    }
    
    public struct PlayerInput : IComponentData
    {
        public InputCmd cmd;
        public byte playerId;
    }
}
using Unity.Entities;

namespace SpaceInvaders.Components
{
    public enum DamageType : byte
    {
        DamageMonsters = 0,
        DamagePlayers = 1
    }
    
    public struct Health : IComponentData
    {
        public int health;
        public DamageType absorbDmg;
    }
}
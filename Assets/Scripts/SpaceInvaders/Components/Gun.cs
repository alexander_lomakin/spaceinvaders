using Unity.Entities;

namespace SpaceInvaders.Components
{
    public struct Gun : IComponentData
    {
        public float shotTimer;
        public float shotDuration;
        public float bulletSpeed;
        public int damage;
        public DamageType dmgType;
    }
}
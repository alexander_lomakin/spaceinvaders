using SpaceInvaders.Components;
using SpaceInvaders.Data;
using SpaceInvaders.Systems;
using SpaceInvaders.Ui;
using SpaceInvaders.Utils;
using Unity.Entities;
using UnityEngine;
using Random = UnityEngine.Random;

namespace SpaceInvaders
{
    // All ingame logic controller
    public class GameLoop : MonoBehaviour
    {
        public SpaceshipInfo playerSpaceshipInfo;
        public LevelDetails levelDetails;
        public PlayerBattleHud hud;
        public GameOverScreen gameOverScreen;
        public Pool spaceshipsPool;
        public Pool bulletsPool;

        private IScoreStorage _scoreStorage;
        private bool _bossSpawned;
        
        // Initialize all systems here
        public void StartBattle()
        {
            World.Active.CreateSystem(typeof(InputSystem));
            World.Active.CreateSystem(typeof(InputProcessingSystem));
            World.Active.GetOrCreateSystem(typeof(GunFireSystem));
            World.Active.CreateSystem(typeof(MonsterMoverSystem));
            World.Active.CreateSystem(typeof(RandomMonsterShootSystem));
            World.Active.CreateSystem(typeof(BossProcessingSystem));
            World.Active.CreateSystem(typeof(KillNearBordersSystem));
            World.Active.CreateSystem(typeof(MovementSystem));
            World.Active.CreateSystem(typeof(BulletCollisionDetectionSystem));
            World.Active.CreateSystem(typeof(SpaceshipCollisionSystem));
            World.Active.CreateSystem(typeof(SpaceshipRendererSystem));
            World.Active.CreateSystem(typeof(BulletRendererSystem));
            World.Active.CreateSystem(typeof(WaveSpawnerSystem));

            var spaceshipRenderer = World.Active.GetExistingSystem(typeof(SpaceshipRendererSystem)) as SpaceshipRendererSystem;
            var bulletRenderer = World.Active.GetExistingSystem(typeof(BulletRendererSystem)) as BulletRendererSystem;
            var movementSystem = World.Active.GetExistingSystem(typeof(MovementSystem)) as MovementSystem;
            var monsterMoverSystem = World.Active.GetExistingSystem(typeof(MonsterMoverSystem)) as MonsterMoverSystem;
            var bulletCollisionSystem = World.Active.GetExistingSystem((typeof(BulletCollisionDetectionSystem))) as BulletCollisionDetectionSystem;
            var waveSpawnerSystem = World.Active.GetExistingSystem(typeof(WaveSpawnerSystem)) as WaveSpawnerSystem;
            var killableBorderSystem = World.Active.GetExistingSystem(typeof(KillNearBordersSystem)) as KillNearBordersSystem;
            var randomMonsterShotSystem = World.Active.GetExistingSystem(typeof(RandomMonsterShootSystem)) as RandomMonsterShootSystem;
            
            spaceshipRenderer.spaceshipViewsPool = spaceshipsPool;
            bulletRenderer.bulletViewsPool = bulletsPool;
            movementSystem.levelDetails = levelDetails;
            monsterMoverSystem.moveNextRowDistance = levelDetails.monsterMoveNextRowDistance;
            monsterMoverSystem.levelDetails = levelDetails;
            killableBorderSystem.levelDetails = levelDetails;
            waveSpawnerSystem.spawnBonusShipInterval = levelDetails.spawnBonusShipInterval;
            waveSpawnerSystem.SpawnWave += WaveSpawnerSystemOnSpawnWave;
            waveSpawnerSystem.SpawnBonusShip += WaveSpawnerSystemOnSpawnBonusShip;
            
            killableBorderSystem.Initialize();
            movementSystem.Initialize();
            monsterMoverSystem.Initialize();
            randomMonsterShotSystem.Initialize(levelDetails.monsterShootInterval, levelDetails.monsterRowSize, levelDetails.monsterShotChance);

            _scoreStorage = bulletCollisionSystem;
            
            hud.Initalize(bulletCollisionSystem);
            
            SpaceshipFactory.CreatePlayer(World.Active, 0, levelDetails.startPlayerPositions[0], Vector2.up, playerSpaceshipInfo);
        }

        private void Update()
        {
            if (_scoreStorage == null || _scoreStorage.PlayerLives != 0)
            {
                if (!_bossSpawned && ((Input.GetKey(KeyCode.S) && Input.GetKey(KeyCode.B)) || (_scoreStorage != null && _scoreStorage.KillScore >= 3000)))
                {
                    _bossSpawned = true;
                    SpawnBoss();
                }
                
                return;
            }

            hud.gameObject.SetActive(false);
            gameOverScreen.Initialize(_scoreStorage.KillScore);
            gameObject.SetActive(false);
            gameOverScreen.gameObject.SetActive(true);
        }

        private void WaveSpawnerSystemOnSpawnBonusShip()
        {
            Vector2 position = Vector2.zero;

            var moveLeft = Random.Range(0, 1f) > 0.5f;
            
            if (moveLeft)
            {
                position = new Vector2(levelDetails.lowerRightBorder.x - 0.1f, levelDetails.bonusShipStartY);
            }
            else
            {
                position = new Vector2(levelDetails.upperLeftBorder.x + 0.1f, levelDetails.bonusShipStartY);
            }
            
            var bonusShip = SpaceshipFactory.CreatePlayer(World.Active, SpaceshipFactory.MonsterId, position, Vector2.up, levelDetails.bonusSpaceshipInfo);
            
            World.Active.EntityManager.SetComponentData(bonusShip, new Velocity()
            {
                velocity = moveLeft ? Vector2.left : Vector2.right,
                slowdownSpeed = levelDetails.bonusSpaceshipInfo.slowdownSpeed,
                maxSpeed = levelDetails.bonusSpaceshipInfo.maxSpeed,
                speed = levelDetails.bonusSpaceshipInfo.maxSpeed
            });
        }

        private void WaveSpawnerSystemOnSpawnWave()
        {
            SpawnMonsters();
            SpawnGuards();
        }

        private void SpawnBoss()
        {
            var world = World.Active;
            var moveMonsterSystem = world.GetExistingSystem(typeof(MonsterMoverSystem));

            moveMonsterSystem.Enabled = false;
            
            var bossProcessingSystem = world.GetExistingSystem(typeof(BossProcessingSystem)) as BossProcessingSystem;
            
            bossProcessingSystem.Initialize(levelDetails);
        }

        private void SpawnMonsters()
        {
            int start_y = 0;
            var levelCenter = levelDetails.LevelCenter;
            var monsterHeight = levelDetails.monsterRowHeight;
            var monsterWidth = levelDetails.monsterRowWidth;

            var startPosX = levelCenter.x - (monsterWidth * levelDetails.monsterRowSize) / 2;

            foreach (var monster in levelDetails.monsterRows)
            {
                for (var i = 0; i < levelDetails.monsterRowSize; i++)
                {
                    var posX = startPosX + i * monsterWidth;
                    var posY = levelDetails.monsterStartY + start_y * monsterHeight;
                    var position = new Vector2(posX, posY);

                    SpaceshipFactory.CreateMonster(World.Active, position, Vector2.down, (short)i, monster);
                }

                start_y++;
            }
        }

        private void SpawnGuards()
        {
            foreach (var position in levelDetails.guardStarshipPositions)
            {
                SpaceshipFactory.CreateGuardSpaceship(World.Active, position, Vector2.up, levelDetails.guardSpaceshipInfo);
            }
        }
    }
}
using SpaceInvaders.Components;
using SpaceInvaders.Data;
using Unity.Entities;
using UnityEngine;

namespace SpaceInvaders
{
    public static class SpaceshipFactory
    {
        public const short MonsterId = -1;
        
        public static Entity CreatePlayer(World world, short player, Vector2 startPosition, Vector2 lookDirection, SpaceshipInfo info)
        {
            var entityManager = world.EntityManager;
            var entity = CreateSpaceship(world, startPosition, lookDirection, info);
            
            entityManager.AddComponent(entity, new ComponentType(typeof(SpaceshipDriver), ComponentType.AccessMode.ReadOnly));
            
            entityManager.SetComponentData(entity, new SpaceshipDriver()
            {
                playerId = player
            });

            return entity;
        }

        public static void CreateMonster(World world, Vector2 startPosition, Vector2 lookDirection, short rowId, SpaceshipInfo info)
        {
            var entityManager = world.EntityManager;
            var entity = CreateSpaceship(world, startPosition, lookDirection, info);
            
            entityManager.AddComponent(entity, new ComponentType(typeof(SpaceshipDriver), ComponentType.AccessMode.ReadOnly));
            
            entityManager.SetComponentData(entity, new SpaceshipDriver()
            {
                playerId = MonsterId
            });
            
            entityManager.AddComponent(entity, new ComponentType(typeof(Row), ComponentType.AccessMode.ReadOnly));
            
            entityManager.SetComponentData(entity, new Row()
            {
                rowIndex = rowId
            });
        }
        
        public static Entity CreateBoss(World world, Vector2 startPosition, Vector2 lookDirection, SpaceshipInfo info)
        {
            var entityManager = world.EntityManager;
            var entity = CreateSpaceship(world, startPosition, lookDirection, info);
            
            entityManager.AddComponent(entity, new ComponentType(typeof(SpaceshipDriver), ComponentType.AccessMode.ReadOnly));
            
            entityManager.SetComponentData(entity, new SpaceshipDriver()
            {
                playerId = MonsterId
            });

            return entity;
        }
        
        public static void CreateGuardSpaceship(World world, Vector2 startPosition, Vector2 lookDirection, SpaceshipInfo info)
        {
            CreateSpaceship(world, startPosition, lookDirection, info);
        }
        
        // Instantiate spaceships from script for rapid prototype purposes
        private static Entity CreateSpaceship(World world, Vector2 startPosition, Vector2 lookDirection, SpaceshipInfo info)
        {
            var entityManager = world.EntityManager;
            var entity = entityManager.CreateEntity();
            
            entityManager.AddComponent(entity, new ComponentType(typeof(PositionAndRotation)));
            entityManager.AddComponent(entity, new ComponentType(typeof(Velocity)));
            entityManager.AddComponent(entity, new ComponentType(typeof(ShipShell)));
            entityManager.AddComponent(entity, new ComponentType(typeof(Size)));
            entityManager.AddComponent(entity, new ComponentType(typeof(Gun)));
            entityManager.AddComponent(entity, new ComponentType(typeof(Health)));
            entityManager.AddComponent(entity, new ComponentType(typeof(Score)));

            if (info.killableByBorders)
            {
                entityManager.AddComponent(entity, new ComponentType(typeof(KillableByBorders)));
            }

            entityManager.SetComponentData(entity, new ShipShell()
            {
                spaceshipType = info.shell.type
            }); 
            
            entityManager.SetComponentData(entity, new PositionAndRotation()
            {
                position = startPosition,
                rotation = lookDirection
            });
            
            entityManager.SetComponentData(entity, new Velocity()
            {
                maxSpeed = info.maxSpeed,
                slowdownSpeed = info.slowdownSpeed
            });
            
            entityManager.SetComponentData(entity, new Size()
            {
                width = info.size.x,
                height = info.size.y
            });
            
            entityManager.SetComponentData(entity, new Gun()
            {
                shotDuration = info.gunCooldown,
                bulletSpeed = info.gunBulletSpeed,
                damage = info.damage,
                dmgType = info.dmgType,
                shotTimer = info.gunCooldown
            });

            entityManager.SetComponentData(entity, new Health()
            {
                health = info.health,
                absorbDmg = info.absorbDmg
            });
            
            entityManager.SetComponentData(entity, new Score()
            {
                value = info.score
            });
            
            return entity;
        }
    }
}
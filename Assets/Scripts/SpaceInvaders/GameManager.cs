﻿using SpaceInvaders.Ui;
using UnityEngine;

namespace SpaceInvaders
{
    // All game sybsystems handler
    public class GameManager : MonoBehaviour
    {
        public GameLoop gameLoop;
        public StartScreen startScreen;
        public PlayerBattleHud playerBattleHud;
        
        private void Awake()
        {
            startScreen.StartGame += StartScreenOnStartGame;
            
            ProgramStartPoint();
        }

        private void ProgramStartPoint()
        {
            startScreen.gameObject.SetActive(true);
        }
        
        private void StartScreenOnStartGame()
        {
            gameLoop.StartBattle();
            playerBattleHud.gameObject.SetActive(true);
        }
    }
}

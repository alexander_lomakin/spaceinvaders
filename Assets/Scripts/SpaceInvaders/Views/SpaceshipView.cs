using SpaceInvaders.Components;
using SpaceInvaders.Data;
using UnityEngine;

namespace SpaceInvaders.Views
{
    public class SpaceshipView : MonoBehaviour
    {
        public SpriteRenderer spRenderer;
        public Spaceships spaceshipsViewInfo;

        [HideInInspector]
        public bool entityExistFlag; // ToDo: flag to handle if view has entity
        
        public void Initialize(SpaceshipType type)
        {
            var shellInfo = spaceshipsViewInfo.GetShell(type);

            spRenderer.sprite = shellInfo.shellSprite;

            entityExistFlag = true;
        }
    }
}
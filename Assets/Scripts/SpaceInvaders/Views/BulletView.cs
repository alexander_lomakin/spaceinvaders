using UnityEngine;

namespace SpaceInvaders.Views
{
    public class BulletView : MonoBehaviour
    {
        public SpriteRenderer spRenderer;
        
        [HideInInspector]
        public bool entityExistFlag;
    }
}
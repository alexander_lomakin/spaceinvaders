Project developed in Unity 2019.2.6f1 Personal.

Used default 2d packages, also used:
- 2d Pixel Perfect
- Entities

Recommended to run in Maximized free aspect mode. Press S + B to spawn boss.

In this prototype I tryed to used Data oriented way instead of object oriented. It is new concept for me, so some rude errors maybe exist.
Also I implement my own binding of entity and SpriteRenderer, because I didn't find analogs in ecs documentation. My goal was to show how I can work in new environment. All time required to develop app can be tracked via commit dates + 1 hour of preparations and watching original
 arcade gameplay.